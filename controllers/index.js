"use strict";

const express = require("express");
var router = express.Router();

router.get("/login", require("./login"));
router.get("/callback", require("./callback"));

module.exports = router;
